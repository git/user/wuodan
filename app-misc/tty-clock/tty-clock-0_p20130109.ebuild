# Copyright 1999-2013 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2
# $Header: $

EAPI=5

inherit vcs-snapshot toolchain-funcs eutils

DESCRIPTION="Analog clock in ncurses"
HOMEPAGE="https://github.com/xorg62/tty-clock"
GIT_COMMIT=bfa898579db99bb8c7c47e39ec2c7c9009c7b9cc
SRC_URI="https://github.com/xorg62/tty-clock/archive/${GIT_COMMIT}.tar.gz -> ${P}.tar.gz"

LICENSE="BSD"
SLOT="0"
KEYWORDS="~amd64"

RDEPEND="sys-libs/ncurses"
DEPEND="${RDEPEND}
	virtual/pkgconfig"

src_prepare() {
	tc-export CC
}

src_install() {
	emake \
		DESTDIR="${D}" \
		INSTALLPATH="/usr/bin/" \
		install
	dodoc README
}
